import React from 'react';
import ProizvodKomponenta from './ProizvodKomponenta';
import { connect } from 'react-redux';
import FilterProizvoda from './FilterProizvoda';
import { RootAppState } from '../store';
import { Proizvod } from '../models/Proizvod';

interface StateProps
{
    korisnickoImePrijavljenogKorisnika: string,
    proizvodi: Proizvod[];
}

interface ActionProps
{//ovde mozda ubacim neku akciju kad se tipa menja proizvod, itd itd

}

type Props = StateProps & ActionProps;

class PonudaSvihProizvoda extends React.Component<Props>
{
    render()
    {
        return(
            <div className="container">
                <FilterProizvoda />
                <div className="col-mt">
                    {
                        this.props.proizvodi.map(proizvod => {
                            let vlasnikJePrijavljen: boolean = this.props.korisnickoImePrijavljenogKorisnika === proizvod.korisnickoImeVlasnika;
                            return (<ProizvodKomponenta proizvod={proizvod} vlasnikJePrijavljen={vlasnikJePrijavljen}/>)
                        })
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (rootReducer: RootAppState): StateProps => {
    const { proizvodi } = rootReducer;
    const { korisnickiInterfejs } = rootReducer;
    return {
        korisnickoImePrijavljenogKorisnika: korisnickiInterfejs.korisnickoIme,
        proizvodi: proizvodi.listaSvihProizvoda.filter( (proizvod: Proizvod) => {
            if(proizvodi.objekatFiltriranja.gornjaCena !== 0 || proizvodi.objekatFiltriranja.kategorija !== "")
            {
                if(proizvod.cena > proizvodi.objekatFiltriranja.gornjaCena)
                    return false;
                if(proizvod.kategorija !== proizvodi.objekatFiltriranja.kategorija)
                    return false;
               return true;
            }
            else
                return true;
        })
    }
}

export default connect(mapStateToProps, null)(PonudaSvihProizvoda);