import React from 'react';
import "./Proizvod.css";
import { Dispatch } from 'redux';
import { A_UklanjanjeProizvoda } from '../store/proizvodi/akcije';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Proizvod } from '../models/Proizvod';

interface StoreProps //propertiji ipak mogu da se dobijaju od napolje...
{
    proizvod: Proizvod,
    vlasnikJePrijavljen: boolean //ovo moze da pomogne za uslovno renderovanje
}

interface ActionProps
{
    obrisiProizvod: typeof A_UklanjanjeProizvoda
}

type Props = StoreProps & ActionProps;

interface State
{
    preusmeriNaIzmenu: boolean
}

//preimenovano zbog istoimenog tipa
class ProizvodKomponenta extends React.Component<Props, State>
{
    state = {
        preusmeriNaIzmenu: false
    };

    render()
    {
        const { proizvod } = this.props;
           return(
            <div className="col-md-6">
                <figure className="card card-product">
                    <div className="card">
                        <div className="card-header">
                            <span className="card-title">{proizvod.naziv}</span> <br/>
                            <span className="price-new">Cena: {proizvod.cena} RSD</span> <br/>
                            <span className="card-title">Objavio: {proizvod.korisnickoImeVlasnika}</span>
                        </div>
                        <div className="img-wrap">
                            <img src={proizvod.putanjaDoSlike} 
                                 className="card-img" 
                                 alt="Nema slike"/>
                        </div>
                        <figcaption className="info-wrap">
                            <p className="desc">Kategorija: {proizvod.kategorija}</p>
                            <p className="desc">Opis: {proizvod.opis}</p>
                        </figcaption>
                        <div className="bottom-wrap">
                            {   
                                this.props.vlasnikJePrijavljen? <React.Fragment>
                                                            <Link className="btn btn-warning" to={`/izmeni-proizvod/${proizvod.id}`}>Izmeni</Link>
                                                            <button className="btn btn-danger" onClick={this.obrisiProizvod}>Obriši</button>
                                                        </React.Fragment> : 
                                                        <React.Fragment/>
                            }
                        </div>
                    </div>
                </figure>           
            </div>
        )
    }

    obrisiProizvod = () => {
        const { proizvod } = this.props;
        console.log(proizvod);//nesto zeza
        this.props.obrisiProizvod({
            id: proizvod.id,
            korisnickoImeVlasnika: proizvod.korisnickoImeVlasnika,
            naziv: proizvod.naziv,
            kategorija: proizvod.kategorija,
            cena: proizvod.cena
        });
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        obrisiProizvod: (proizvod: Proizvod) => dispatch(A_UklanjanjeProizvoda(proizvod))
    }
}

export default connect(null, mapDispatchToProps)(ProizvodKomponenta);