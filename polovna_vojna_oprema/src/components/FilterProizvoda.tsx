import React from 'react';
import { A_FiltriranjeProizvoda } from '../store/proizvodi/akcije';
import { Dispatch } from 'redux';
import { FilterObjekat } from '../store/proizvodi/tipovi';
import { connect } from 'react-redux';

interface Props
{
    filtrirajProizvode: typeof A_FiltriranjeProizvoda
}

interface State
{
    gornjaCena: number,
    kategorija: string
}

class FilterProizvoda extends React.Component<Props, State>
{
    state = {
        gornjaCena: 0,
        kategorija: '',
    };

    render(): JSX.Element
    {
        return(
        <div className="navbar navbar-default">
            <div className="row">
                <label>Cena do:</label> 
                <input type="number" 
                       name="inputCenaDo" 
                       onChange={this.onChangeCenaDo}/>
                <label>Kategorija:</label>
                <select name="selectKategorija" onChange={this.onChangeKategorija}>
                    <option value=''>Bez filtera</option>
                    <option value="Puska">Puška</option>
                    <option value="Bomba">Bomba</option>
                </select>
                <button className="btn btn-success" 
                        onClick={this.filtrirajProizvode}>
                        Primeni filtere
                </button>
            </div>
        </div>);
    }

    onChangeCenaDo = (event: React.ChangeEvent<HTMLInputElement>) => this.setState({gornjaCena: parseInt(event.target.value)});
    onChangeKategorija = (event: React.ChangeEvent<HTMLSelectElement>) => this.setState({kategorija: event.target.value});

    filtrirajProizvode = () => {
        this.props.filtrirajProizvode({
            gornjaCena: this.state.gornjaCena,
            kategorija: this.state.kategorija
        })
    }
}

const mapDispatchToProps = (dispatch: Dispatch): Props => {
    return {
        filtrirajProizvode: (filterObjekat: FilterObjekat) => dispatch(A_FiltriranjeProizvoda(filterObjekat))
    }
}

export default connect(null, mapDispatchToProps)(FilterProizvoda);