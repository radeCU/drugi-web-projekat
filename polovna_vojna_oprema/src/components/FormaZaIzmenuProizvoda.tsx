import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { History } from 'history';
import { A_IzmeniProizvod } from '../store/proizvodi/akcije';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { RootAppState } from '../store';
import { Proizvod } from '../models/Proizvod';

interface PropertijiOdRutera extends RouteComponentProps<{idProizvoda: string}>
{
    history: History
}

interface StateProps
{
    proizvodi: Proizvod[]
}

interface ActionProps
{
    izmeniProizvod: typeof A_IzmeniProizvod;
}

type Props = PropertijiOdRutera & StateProps & ActionProps;

interface State
{
    novaCena: number;
    noviOpis: string
}

class FormaZaIzmenuProizvoda extends React.Component<Props, State>
{
    constructor(props: Props)
    {
        super(props);
        this.state = {
            novaCena: 0,
            noviOpis: ''
        };
    }

    render() {
        // const {params} = this.props.match;
        return (
            <React.Fragment>
                <div className="col-sm-6 offset-sm-3 text-center">
                <div className="form-group">
                    <h1>Izmena proizvoda:</h1>
                    <input className="form-control" 
                           type="number" 
                           name="inputNovaCena" 
                           onChange={this.onChangeCena} 
                           placeholder="Unesi novu cenu"/><br/>
                    <textarea className="form-control" 
                              name="textareaSifraKorisnika" 
                              onChange={this.onChangeOpis}>
                            {"Neki opis"}
                    </textarea><br/>
                </div>
                <div className="form-group">
                    <button className="btn btn-primary btn-lg" 
                            onClick={this.potvrdiIzmene}>
                            Potvrdi
                    </button>
                    <Link className="btn btn-info btn-lg" to="/ponuda">Nazad</Link> 
                </div>
            </div>
            </React.Fragment>
        );
    }

    onChangeOpis = (event: React.ChangeEvent<HTMLTextAreaElement>): void => this.setState({noviOpis: event.target.value});

    onChangeCena = (event: React.ChangeEvent<HTMLInputElement>): void => this.setState({novaCena: parseInt(event.target.value)});

    potvrdiIzmene = () => {
        let noviStariProizvod = {
            id: this.props.match.params.idProizvoda,
            cena: this.state.novaCena,
            opis: this.state.noviOpis
        };
        console.log(this.props.match.params.idProizvoda);
        console.log(noviStariProizvod);
        this.props.izmeniProizvod(noviStariProizvod);
        this.props.history.push("/ponuda");
    }
}

const mapStateToProps = (rootReducer: RootAppState): StateProps => {
    return {
        proizvodi: rootReducer.proizvodi.listaSvihProizvoda
    };
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        izmeniProizvod: (izmenjeniProizvod: Proizvod) => dispatch(A_IzmeniProizvod(izmenjeniProizvod))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(FormaZaIzmenuProizvoda);