import React from 'react';
import shortid from 'shortid';
import { Dispatch } from 'redux';
import { History } from 'history';
import { A_DodajProizvod } from '../store/proizvodi/akcije';
import { connect } from 'react-redux';

import bomba from '../slike/bomba.jpg';
import puska from '../slike/puska.jpg';
import artiljerija from '../slike/artiljerija.jpg';
import { RootAppState } from '../store';
import { Proizvod } from '../models/Proizvod';

interface HistoryProps
{
    history: History
}

interface StoreProps
{
    korisnickoImePrijavljenogKorisnika: string
}

interface ActionProps
{
    dodajProizvod: typeof A_DodajProizvod
}

type Props = StoreProps & ActionProps & HistoryProps;

interface State
{
    naziv: string,
    kategorija: string,
    opis?: string, 
    cena: number,
    putanjaDoSlike: string
}

class FormaZaDodavanjeOglasa extends React.Component<Props, State>
{
    state = {
        naziv: '',
        kategorija: 'Puska',
        opis: '',
        cena: 0,
        putanjaDoSlike: ''
    };

    render()
    {
        return (
            <div className="row">
            <div className="col-sm-6 offset-sm-3 text-center">
                    <h3>Postavi oglas</h3>

                    <div className="form-group">
                        <input type="text" 
                               name="inputNaziv" 
                               className="form-control" 
                               placeholder="Unesi naziv" 
                               onChange={this.onChangeNaziv}/>
                        <label className="control-label">Kategorija predmeta:</label>
                        <select className="form-control" onChange={this.onChangeKategorija}>
                            <option value="Puska">Puška</option>
                            <option value="Bomba">Bomba</option>
                            <option value="Artiljerija">Artiljerija</option>
                        </select>
                        <label className="control-label">Opis:</label>
                        <textarea name="textareaOpis" 
                                  className="form-control" 
                                  onChange={this.onChangeOpis}/>
                        <label className="control-label">Cena:</label>
                        <input type="number" 
                               name="inputCena" 
                               className="form-control" 
                               onChange={this.onChangeCena}/>
                    </div>
                    
                    <div className="form-group">
                        <button className="btn btn-primary btn-lg" 
                                onClick={this.postaviOglas}>
                                Postavi oglas
                        </button>
                    </div>
                
            </div>
        </div>
        );
    }

    onChangeNaziv = (event: React.ChangeEvent<HTMLInputElement>) => this.setState({naziv: event.target.value});

    onChangeOpis = (event: React.ChangeEvent<HTMLTextAreaElement>) => this.setState({opis: event.target.value});

    onChangeCena = (event: React.ChangeEvent<HTMLInputElement>) => this.setState({cena: parseInt(event.target.value)});//ne konvertuje ga automatski izgleda

    onChangeKategorija = (event: React.ChangeEvent<HTMLSelectElement>) => this.setState({kategorija: event.target.value});

    postaviOglas = (): void => { 
        this.props.dodajProizvod({
            id: shortid.generate(),
            korisnickoImeVlasnika: this.props.korisnickoImePrijavljenogKorisnika,
            naziv: this.state.naziv,
            kategorija: this.state.kategorija,
            opis: this.state.opis,
            cena: this.state.cena,
            putanjaDoSlike: this.odrediSlikuZaKategoriju(this.state.kategorija)
        });
        alert(`Uspešno dodavanje proizvoda`);
        this.props.history.push("/ponuda");
    }

    odrediSlikuZaKategoriju = (prosledjenaKategorija: string): string => {
        switch(prosledjenaKategorija)
        {
            case 'Bomba':
                return bomba;
            case 'Puska':
                return puska;
            case 'Artiljerija':
                return artiljerija;           
            default:
                return '';
        }
    }
}

const mapStateToProps = (rootReducer: RootAppState): StoreProps => {
    return {
        korisnickoImePrijavljenogKorisnika: rootReducer.korisnickiInterfejs.korisnickoIme
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        dodajProizvod: (proizvod: Proizvod) => dispatch(A_DodajProizvod(proizvod))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormaZaDodavanjeOglasa);