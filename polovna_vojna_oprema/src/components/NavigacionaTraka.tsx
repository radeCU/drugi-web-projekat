import React from 'react';
import { Link } from 'react-router-dom';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { A_OdjaviKorisnika } from '../store/korisnici/akcije';
import { RootAppState } from '../store';


interface StoreProps
{
    nekoJePrijavljen: boolean,
    prijavljenoKorisnickoIme: string
};

interface ActionProps
{
    odjaviKorisnika: typeof A_OdjaviKorisnika;
}

type Props = StoreProps & ActionProps;

class NavigacionaTraka extends React.Component<Props>
{
    render(): JSX.Element
    {
        const prikazZaPrijavljene: JSX.Element = (
            <React.Fragment>
                <Link to="/dodaj-oglas" 
                     className="navbar-brand">
                    Dodaj oglas
                </Link>
                <Link to="/prijava" 
                      className="navbar-brand" 
                      onClick={this.props.odjaviKorisnika}>
                    <span>({this.props.prijavljenoKorisnickoIme})</span>
                    Odjavi se
                </Link>
            </React.Fragment>
        );

        const prikazZaNeprijavljene: JSX.Element = (
            <React.Fragment>
                <Link to="/" 
                      className="navbar-brand">
                    Napravi nalog
                </Link>
                <Link to="/prijava" 
                      className="navbar-brand">
                    Prijavi se
                </Link>
            </React.Fragment>
        );
        
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <h3>Polovna vojna oprema - mesto gde Ženevska konvencija ne važi</h3>
                    <div className="navbar-header">                        
                        <Link to="/ponuda" 
                              className="navbar-brand">
                            Ponuda
                        </Link>  
                        { this.props.nekoJePrijavljen? prikazZaPrijavljene : prikazZaNeprijavljene }
                        <Link to="/o-nama" 
                              className="navbar-brand">
                            O nama
                        </Link>
                        <a href="https://en.wikipedia.org/wiki/Geneva_Conventions" 
                           className="navbar-brand" 
                           target="_blank">
                            Dobar vic
                        </a>
                    </div>
                </div>
            </nav>
        )
    }
}

const mapStateToProps = (rootReducer: RootAppState): StoreProps => {
    return {
        nekoJePrijavljen: rootReducer.korisnickiInterfejs.nekoJePrijavljen,
        prijavljenoKorisnickoIme: rootReducer.korisnickiInterfejs.korisnickoIme
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        odjaviKorisnika: () => dispatch(A_OdjaviKorisnika())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigacionaTraka);