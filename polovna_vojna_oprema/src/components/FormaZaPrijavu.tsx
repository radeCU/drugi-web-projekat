import React from 'react';
import { Dispatch } from 'redux';
import { A_PrijavaKorisnika } from '../store/korisnici/akcije';
import { connect } from 'react-redux';
import { PodaciZaPrijavu } from '../store/korisnici/tipovi';
import { RootAppState } from '../store';
import { Redirect } from 'react-router-dom';

interface State
{
    korisnickoIme: string,
    lozinka: string
}

interface StoreProps
{
    korisnickoIme: string,
    korisnickoImeJePogresno: boolean,
    lozinkaJePogresna: boolean,
    zahtevSeObradjuje: boolean,
    korisnikJePrijavljen: boolean
}

interface ActionProps
{
    prijaviKorisnika: typeof A_PrijavaKorisnika
}

type Props = StoreProps & ActionProps;

class FormaZaPrijavu extends React.Component<Props, State>
{
    state = {
        korisnickoIme: '',
        lozinka: '',
    };

    render(): JSX.Element
    {
        const { korisnickoImeJePogresno, 
                lozinkaJePogresna, 
                zahtevSeObradjuje, 
                korisnikJePrijavljen } = this.props;

        if(korisnikJePrijavljen)
        {
            alert("Prijava je uspela");
            return <Redirect exact to="/ponuda"/>;
        }
        else
        return (
            <div className="col-sm-6 offset-sm-3 text-center">
                <div className="form-group">
                    <h1>Prijavi se:</h1>
                    <input type="text" 
                           name="inputKorisnickoIme" 
                           placeholder="Uneti korisničko ime" 
                           onChange={this.onChangeKorisnickoIme}/><br/>
                    { 
                        korisnickoImeJePogresno &&
                        <div style={{color:"red"}}> Nepostojeće korisničko ime</div>
                    }
                    <input type="password" 
                           name="inputSifraKorisnika" 
                           placeholder="Uneti šifru" 
                           onChange={this.onChangeLozinka}/><br/>
                    {
                        lozinkaJePogresna && 
                        <div style={{color:"red"}}> Lozinka je pogrešna </div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary btn-lg" 
                            onClick={this.prijaviSe}
                            disabled={zahtevSeObradjuje}>
                            Potvrdi
                    </button>
                </div>
            </div>
        );
    }

    onChangeKorisnickoIme = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({korisnickoIme: event.target.value});
    }

    onChangeLozinka = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({lozinka: event.target.value});
    }

    prijaviSe = (): void => {
        const podaciZaPrijavu: PodaciZaPrijavu = {
            korisnickoIme: this.state.korisnickoIme,
            lozinka: this.state.lozinka
        };

        this.props.prijaviKorisnika(podaciZaPrijavu);
    }
}

const mapStateToProps = (rootReducer: RootAppState): StoreProps => {
    return {
        korisnickoIme: rootReducer.korisnici.korisnickoIme, 
        korisnickoImeJePogresno: rootReducer.korisnici.korisnickoImeJePogresno,
        lozinkaJePogresna: rootReducer.korisnici.lozinkaJePogresna,
        zahtevSeObradjuje: rootReducer.korisnici.zahtevSeObradjuje,
        korisnikJePrijavljen: rootReducer.korisnickiInterfejs.nekoJePrijavljen
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        prijaviKorisnika: (podaci: PodaciZaPrijavu) => dispatch(A_PrijavaKorisnika(podaci))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormaZaPrijavu);