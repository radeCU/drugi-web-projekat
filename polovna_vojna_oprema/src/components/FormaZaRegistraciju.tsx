import React, { ChangeEvent } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { A_RegistrujKorisnika } from '../store/korisnici/akcije';
import { History } from 'history';
import { RootAppState } from '../store';
import { Korisnik } from '../models/Korisnik';

interface StoreProps
{
    prijavaSeObradjuje: boolean,
    korisnickoImeJeZauzeto: boolean
};

interface ActionProps
{
    registrujKorisnika: typeof A_RegistrujKorisnika
}

type Props = StoreProps & ActionProps

interface State
{
    ime: string,
    prezime: string,
    korisnickoIme: string,
    lozinka: string
};

class FormaZaRegistraciju extends React.Component<Props, State>
{
    readonly state = {
        ime: '',
        prezime: '',
        korisnickoIme: '',
        lozinka: ''
    };

    render(): JSX.Element
    {
        //trebam neki redirect da odradim ako uspe prijava
        const { korisnickoImeJeZauzeto, prijavaSeObradjuje } = this.props;
        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3 text-center">
                    <h3>Pridruži se našem sajtu</h3>

                    <div className="form-group">
                        <label className="control-label">Ime:</label>
                        <input type="text" 
                                name="inputIme" 
                                className="form-control" 
                                onChange={this.onChangeIme}/>
                        <label className="control-label">Prezime:</label>
                        <input type="text" 
                                name="inputPrezime" 
                                className="form-control" 
                                onChange={this.onChangePrezime}/>
                        <label className="control-label">Korisničko ime:</label>
                        <input type="text" 
                                name="inputKorisnickoIme" 
                                className="form-control" 
                                onChange={this.onChangeKorisnickoIme}/>
                        {
                            korisnickoImeJeZauzeto &&
                            <div style={{color:"red"}}>Korisničko ime je zauzeto</div>
                        }
                        <label className="control-label">Lozinka:</label>
                        <input type="password" 
                                name="inputLozinka" 
                                id="" 
                                className="form-control" 
                                onChange={this.onChangeLozinka}/>
                    </div>
                    
                    <div className="form-group">
                        <button className="btn btn-primary btn-lg" 
                                onClick={this.napraviNalog}
                                disabled={prijavaSeObradjuje}>
                                Napravi nalog
                        </button>
                    </div>
                    
                </div>
            </div>
        );
    }
    
    onChangeIme = (event: ChangeEvent<HTMLInputElement>): void => {
        this.setState({ime: event.target.value});
    }

    onChangePrezime = (event: ChangeEvent<HTMLInputElement>): void => {
        this.setState({prezime: event.target.value});
    }

    onChangeKorisnickoIme = (event: ChangeEvent<HTMLInputElement>): void => {
        this.setState({korisnickoIme: event.target.value});
    }

    onChangeLozinka = (event: ChangeEvent<HTMLInputElement>): void => {
        this.setState({lozinka: event.target.value});
    }

    napraviNalog = (): void => {
        let noviKorisnik: Korisnik = {
            id: -10, /*ovo ce da se unutar sage dodeli, ako je username unikatan*/
            ime: this.state.ime,
            prezime: this.state.prezime,
            korisnickoIme: this.state.korisnickoIme,
            lozinka: this.state.lozinka
        };

        this.props.registrujKorisnika(noviKorisnik);
    }
}

const mapStateToProps = (rootReducer: RootAppState): StoreProps => {
    return {
        prijavaSeObradjuje: rootReducer.korisnici.zahtevSeObradjuje,
        korisnickoImeJeZauzeto: rootReducer.korisnici.korisnickoImeJeZauzeto
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        registrujKorisnika: (podaci: Korisnik) => dispatch(A_RegistrujKorisnika(podaci)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormaZaRegistraciju);