import React from 'react';
import { Router, Route } from 'react-router-dom';
import NavigacionaTraka from './components/NavigacionaTraka';

import FormaZaRegistraciju from './components/FormaZaRegistraciju';
import ONama from './components/ONama';
import FormaZaDodavanjeOglasa from './components/FormaZaDodavanjeOglasa';

import FormaZaPrijavu from './components/FormaZaPrijavu';
import PonudaSvihProizvoda from './components/PonudaSvihProizvoda';
import FormaZaIzmenuProizvoda from './components/FormaZaIzmenuProizvoda';

import createBrowserHistory  from 'history/createBrowserHistory'; //whack-a-mole

const App: React.FC = () => {
  let history = createBrowserHistory(); //i ovo ce kao da radi...
  return (
    <Router history={history}>
      <div className="App">
        <NavigacionaTraka />
        <Route exact path="/" component={FormaZaRegistraciju} />
        <Route exact path="/ponuda" component={PonudaSvihProizvoda} /> 
        <Route exact path="/izmeni-proizvod/:idProizvoda" component={FormaZaIzmenuProizvoda}/>
        <Route exact path="/prijava" component={FormaZaPrijavu}/>
        <Route exact path="/o-nama" component={ONama}/>
        <Route exact path="/dodaj-oglas" component={FormaZaDodavanjeOglasa}/>
      </div>
    </Router>
  );
}

export default App;
