const putanjaDoBaze: string = "http://localhost:3001";//ovde posle idu vazdan konkatenacije
const nazivTabeleKorisnika: string = "korisnici";
const nazivTabeleProizvoda: string = "proizvodi";

export const URLZaKorisnike: string = `${putanjaDoBaze}/${nazivTabeleKorisnika}`;
export const URLZaProizvode: string = `${putanjaDoBaze}/${nazivTabeleProizvoda}`; 