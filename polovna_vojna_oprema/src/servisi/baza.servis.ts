import { Korisnik } from '../models/Korisnik';
import { Proizvod } from '../models/Proizvod';

export function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)
{
    let zaglavljeZahteva: RequestInit = {
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'Accept': 'application/json, text/plain, */*'
        }
    };
    
    if(podaci)
        zaglavljeZahteva.body = JSON.stringify(podaci);
    
    let ishodFetcha = podaci? yield fetch(URL, zaglavljeZahteva) : yield fetch(URL, zaglavljeZahteva);
    if(ishodFetcha.ok)
        return yield ishodFetcha.json();
    else
        return ishodFetcha.status.toString();
}

//u jako davna vremena sam smatrao da ova funkcija ima smisla...
export function vratiKorisnikaPoKorisnickomImenu(nizKorisnika: Korisnik[], korisnickoIme: string)
{
    for(let i=0; i < nizKorisnika.length; i++)
    {
        let korisnik = nizKorisnika[i];
        if(korisnik.korisnickoIme === korisnickoIme)
            return korisnik;
    }
    return null;
}

export function vratiProizvodPoIDu(nizProizvoda: Proizvod[], idProizvoda: string)
{
    for(let i=0; i < nizProizvoda.length; i++)
    {
        let proizvod = nizProizvoda[i];
        if(proizvod.id === idProizvoda)
            return proizvod;
    }
    //nema return null jer ce ovo 300% da vrati objekat zbog nacina obrade
}