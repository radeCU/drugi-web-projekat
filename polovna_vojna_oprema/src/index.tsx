import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import konfigurisiStore from './store/index';
import { ucitajPodatke } from './store/aplikacija/akcije';

const store = konfigurisiStore();
//dobra fora za ucitavanje iz baze
class Root extends React.Component
{
    render()
    {
        store.dispatch(ucitajPodatke());
        return(
            <Provider store={store}>
                <App/>
            </Provider>
        );
    }
}

ReactDOM.render(<Root/>, document.getElementById('root'));