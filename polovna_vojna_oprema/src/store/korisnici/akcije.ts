import { PrijavaKorisnika, AkcijeNadKorisnicima, PodaciZaPrijavu, DodavanjeNovogKorisnika, PrijavaKorisnikaUspeh, DodavanjeNovogKorisnikaUspeh, DodavanjeNovogKorisnikaZauzetoKorisnickoIme } from './tipovi';
import { Korisnik } from '../../models/Korisnik';

export const A_RegistrujKorisnika = (noviKorisnik: Korisnik): DodavanjeNovogKorisnika => {
    return {
        type: AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA,
        korisnik: noviKorisnik 
    }
}

export const A_DodavanjeNovogKorisnikaUspeh = (ime: string, prezime: string, korisnickoIme: string): DodavanjeNovogKorisnikaUspeh => {
    return {
        type: AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA_USPEH,
        ime: ime,
        prezime: prezime,
        korisnickoIme: korisnickoIme
    };
};

export const A_DodavanjeNovogKorisnikaZauzetoKorisnickoIme = (): DodavanjeNovogKorisnikaZauzetoKorisnickoIme => {
    return {
        type: AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA_ZAUZETO_KORISNICKO_IME
    };
};

export const A_PrijavaKorisnika = (podaciZaPrijavu: PodaciZaPrijavu): PrijavaKorisnika => {
    return {
        type: AkcijeNadKorisnicima.PRIJAVA_KORISNIKA,
        korisnickoIme: podaciZaPrijavu.korisnickoIme,
        lozinka: podaciZaPrijavu.lozinka
    }
}

export const A_PrijavaKorisnikaUspeh = (korisnickoIme: string): PrijavaKorisnikaUspeh => {
    return {
        type: AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_USPEH,
        korisnickoIme: korisnickoIme
    };
};

export const A_PrijavaKorisnikaNepostojeceKorisnickoIme = () => {
    return {
        type: AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_LOS_USERNAME
    };
};

export const A_PrijavaKorisnikaPogresnaLozinka = () => {
    return {
        type: AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_LOSA_LOZINKA
    };
};

export const A_DodavanjeNovogKorisnika = (noviKorisnik: Korisnik): DodavanjeNovogKorisnika => {
    return {
        type: AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA,
        korisnik: noviKorisnik
    };
};

export const A_OdjaviKorisnika = () => {
    return {
        type: AkcijeNadKorisnicima.ODJAVA_KORISNIKA
    }
}