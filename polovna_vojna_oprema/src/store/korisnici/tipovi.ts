import { Korisnik } from "../../models/Korisnik";

export enum AkcijeNadKorisnicima
{
    PRIJAVA_KORISNIKA = "[Akcije nad korisnicima] Prijava korisnika",
    PRIJAVA_KORISNIKA_USPEH = "[Akcije nad korisnicima] Prijava korisnika uspeh",
    PRIJAVA_KORISNIKA_LOS_USERNAME = "[Akcije nad korisnicima] Prijava korisnika los username",
    PRIJAVA_KORISNIKA_LOSA_LOZINKA = "[Akcije nad korisnicima] Prijava korisnika losa lozinka",
    DODAVANJE_NOVOG_KORISNIKA = "[Akcije nad korisnicima] Dodavanje novog korisnika",
    DODAVANJE_NOVOG_KORISNIKA_USPEH = "[Akcije na korisnicima] Dodavanje novog korisnika uspeh",
    DODAVANJE_NOVOG_KORISNIKA_ZAUZETO_KORISNICKO_IME = "[Akcije na korisnicima] Dodavanje novog korisnika zauzeto korisnicko ime",
    ODJAVA_KORISNIKA = "[Akcije nad korisnicima] Odjava korisnika"
}

export interface PodaciZaPrijavu
{
    korisnickoIme: string,
    lozinka: string
}

export interface PrijavaKorisnika
{
    type: AkcijeNadKorisnicima,
    korisnickoIme: string,
    lozinka: string
};

export interface PrijavaKorisnikaUspeh
{
    type: AkcijeNadKorisnicima,
    korisnickoIme: string
};

export interface DodavanjeNovogKorisnika
{
    type: AkcijeNadKorisnicima,
    korisnik: Korisnik
};

export interface DodavanjeNovogKorisnikaUspeh
{
    type: AkcijeNadKorisnicima,
    ime: string,
    prezime: string,
    korisnickoIme: string
};

export interface DodavanjeNovogKorisnikaZauzetoKorisnickoIme
{
    type: AkcijeNadKorisnicima
};