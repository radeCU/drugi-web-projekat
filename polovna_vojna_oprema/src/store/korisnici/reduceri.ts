import { AkcijeNadKorisnicima, PrijavaKorisnikaUspeh } from "./tipovi";

export interface StanjeSvihKorisnika
{
    korisnickoIme: string,
    korisnickoImeJePogresno: boolean,
    lozinkaJePogresna: boolean,
    korisnickoImeJeZauzeto: boolean,
    zahtevSeObradjuje: boolean
}

const pocetnoStanje: StanjeSvihKorisnika = {
    korisnickoIme: "",
    korisnickoImeJePogresno: false,
    lozinkaJePogresna: false,
    korisnickoImeJeZauzeto: false,
    zahtevSeObradjuje: false
};

export function reducerKorisnika(stanje = pocetnoStanje, akcija: any) : StanjeSvihKorisnika
{
    switch(akcija.type)
    {
        case AkcijeNadKorisnicima.PRIJAVA_KORISNIKA:
        {
            return {
                ...stanje,
                zahtevSeObradjuje: true
            };
        }
        
        case AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_USPEH:
        {
            let kastovanaAkcija = akcija as PrijavaKorisnikaUspeh;
            return {
                ...stanje,
                korisnickoIme: kastovanaAkcija.korisnickoIme,
                korisnickoImeJePogresno: false,
                lozinkaJePogresna: false
            };
        }    
        
        case AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_LOS_USERNAME:
        {
            return {
                ...stanje,
                korisnickoImeJePogresno: true,
                lozinkaJePogresna: false, /*ista fora kao i korisnicko ime*/
                zahtevSeObradjuje: false
            };
        }

        case AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_LOSA_LOZINKA:
        {
            return {
                ...stanje,
                korisnickoImeJePogresno: false, /*jer korisnicko ime mora a se slaze sa lozinkom*/
                lozinkaJePogresna: true,
                zahtevSeObradjuje: false
            };
        }

        case AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA:
        {
            return {
                ...stanje,
                zahtevSeObradjuje: true,
                korisnickoImeJeZauzeto: false
            };
        }

        case AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA_USPEH:
        {
            return {
                ...stanje,
                zahtevSeObradjuje: false,
                korisnickoImeJeZauzeto: false
            };
        }

        case AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA_ZAUZETO_KORISNICKO_IME:
        {
            return {
                ...stanje,
                zahtevSeObradjuje: false,
                korisnickoImeJeZauzeto: true
            };
        }

        case AkcijeNadKorisnicima.ODJAVA_KORISNIKA:
        {
            return {
                ...stanje,
                zahtevSeObradjuje: false
            };
        }

        default:
            return stanje;
    }
}