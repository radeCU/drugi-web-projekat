import * as saga from 'redux-saga/effects';
import { AkcijeNadKorisnicima, DodavanjeNovogKorisnika, PrijavaKorisnika } from './tipovi' 
import { uputiZahtevKaBazi } from '../../servisi/baza.servis';
import { URLZaKorisnike } from '../../servisi/servisneKonstante';
import { Korisnik } from '../../models/Korisnik';
import { A_DodavanjeNovogKorisnikaUspeh, A_DodavanjeNovogKorisnikaZauzetoKorisnickoIme, A_PrijavaKorisnikaNepostojeceKorisnickoIme, A_PrijavaKorisnikaPogresnaLozinka, A_PrijavaKorisnikaUspeh } from './akcije';

export function* korisnickaSaga()
{
    yield saga.all([saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve() //ovo gleda i reducer i saga
{
    yield saga.takeEvery(AkcijeNadKorisnicima.DODAVANJE_NOVOG_KORISNIKA, dodavanjeNovogKorisnika);
    yield saga.takeEvery(AkcijeNadKorisnicima.PRIJAVA_KORISNIKA, prijaviKorisnika);
}

function* dodavanjeNovogKorisnika(akcija: DodavanjeNovogKorisnika)
{
    const { korisnik } = akcija;
    let noviKorisnik: Korisnik = {
        id: korisnik.id,
        ime: korisnik.ime,
        prezime: korisnik.prezime,
        korisnickoIme: korisnik.korisnickoIme,
        lozinka: korisnik.lozinka
    };
    
    //zbog toga sto je json-server ogranicen, prvo cu get da vidim username dal je unikat, pa onda ide POST
    let ishodUpisa = yield uputiZahtevKaBazi('GET', 
                                             `${URLZaKorisnike}/?korisnickoIme=${noviKorisnik.korisnickoIme}`,
                                              null);
    let korisnikIzBaze = ishodUpisa[0];
    if(korisnikIzBaze)
        yield saga.put(A_DodavanjeNovogKorisnikaZauzetoKorisnickoIme());
    else
    {
        yield uputiZahtevKaBazi("POST", URLZaKorisnike, noviKorisnik);
        yield saga.put(A_DodavanjeNovogKorisnikaUspeh(noviKorisnik.ime, noviKorisnik.prezime, noviKorisnik.korisnickoIme));
    }
}

function* prijaviKorisnika(podaciZaPrijavu: PrijavaKorisnika)
{
    let korisnikNiz: Korisnik[] = yield uputiZahtevKaBazi("GET", 
                                           `${URLZaKorisnike}/?korisnickoIme=${podaciZaPrijavu.korisnickoIme}`,
                                           null);

    let korisnik = korisnikNiz[0];
    if(korisnik && korisnik.korisnickoIme === podaciZaPrijavu.korisnickoIme)
    {
        if(korisnik!.lozinka === podaciZaPrijavu.lozinka)
            yield saga.put(A_PrijavaKorisnikaUspeh(korisnik!.korisnickoIme));
        else
            yield saga.put(A_PrijavaKorisnikaPogresnaLozinka());
    }
    else
        yield saga.put(A_PrijavaKorisnikaNepostojeceKorisnickoIme());
}