import { Korisnik } from "../../models/Korisnik";
import { AkcijeNadKorisnicima } from "../korisnici/tipovi";

const prijaviKorisnika = (korisnik: Korisnik) => {
    return {
        type: AkcijeNadKorisnicima.PRIJAVA_KORISNIKA,
        prijavljeniKorisnik: korisnik,
        
    }
}