import { AkcijeNadKorisnicima, PrijavaKorisnikaUspeh } from '../korisnici/tipovi';

export interface StanjeKorisnickogInterfejsa
{
    nekoJePrijavljen: boolean,
    korisnickoIme: string
}

const pocetnoStanje: StanjeKorisnickogInterfejsa = {
    nekoJePrijavljen: false,
    korisnickoIme: ""
};

export function reducerKorisnickogInterfejsa(stanje = pocetnoStanje, akcija: any) : StanjeKorisnickogInterfejsa
{
    switch(akcija.type)
    {
        case AkcijeNadKorisnicima.PRIJAVA_KORISNIKA_USPEH:
        {
            const { korisnickoIme } = akcija as PrijavaKorisnikaUspeh;
            return {
                ...stanje,
                nekoJePrijavljen: true,
                korisnickoIme: korisnickoIme
            }
        }

        case AkcijeNadKorisnicima.ODJAVA_KORISNIKA:
        {
            return {
                ...stanje,
                nekoJePrijavljen: false,
                korisnickoIme: ""
            }
        }

        default:
            return stanje;
    }
}