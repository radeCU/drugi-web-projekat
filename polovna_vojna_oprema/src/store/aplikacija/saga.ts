import * as saga from 'redux-saga/effects';
import { uputiZahtevKaBazi } from '../../servisi/baza.servis';
import { URLZaProizvode } from '../../servisi/servisneKonstante';
import { UCITAJ_PODATKE } from './tipovi';
import { A_UcitajProizvode } from '../proizvodi/akcije';

export function* sagaAplikacije()
{
    yield saga.all([saga.fork(posmatrajZahteve)]);
}

export function* posmatrajZahteve() //ovo aplikacija sama pokrece po paljenju
{
    yield saga.takeLatest(UCITAJ_PODATKE, ucitajIzBaze); //ne bacam dobar zahtev odnekud...
}

function* ucitajIzBaze()
{
    let proizvodi = yield uputiZahtevKaBazi('GET', `${URLZaProizvode}`, null);

    yield saga.put(A_UcitajProizvode(proizvodi));
}