import { Proizvod } from "../../models/Proizvod";
import {  AkcijeNadProizvodima, DodavanjeProizvoda, FilterObjekat, IzmenaProizvoda, UklanjanjeProizvoda } from "./tipovi";

export const A_UcitajProizvode = (proizvodi: Proizvod[]) => {
    return {
        type: AkcijeNadProizvodima.UCITAVANJE_PROIZVODA_IZ_BAZE,
        proizvodi: proizvodi
    };
}

export const A_DodajProizvod = (noviProizvod: Proizvod): DodavanjeProizvoda =>
{
    return {
        type: AkcijeNadProizvodima.DODAVANJE_PROIZVODA,
        proizvod: noviProizvod
    };
}

//any je, jer me mnogo mrzi da regulisem tip, posto sam veoma ruzno odradio izmenu proizvoda
export const A_IzmeniProizvod = (proizvod: any): IzmenaProizvoda =>
{
    return {
        type: AkcijeNadProizvodima.IZMENA_PROIZVODA,
        idMenjanogProizvoda: proizvod.id,
        cena: proizvod.cena,
        opis: proizvod.opis
    };
}

export const A_UklanjanjeProizvoda = (proizvod: Proizvod): UklanjanjeProizvoda =>
{
    return {
        type: AkcijeNadProizvodima.UKLANJANJE_PROIZVODA,
        proizvod: proizvod
    };
}

export const A_FiltriranjeProizvoda = (filterObjekat: FilterObjekat) => 
{
    return {
        type: AkcijeNadProizvodima.FILTRIRANJE_PROIZVODA,
        filterObjekat: filterObjekat
    };
}
