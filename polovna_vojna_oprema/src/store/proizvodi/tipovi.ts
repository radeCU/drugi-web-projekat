import { Proizvod } from "../../models/Proizvod";

//ovo ce da se uveze tamo u komponenti koja filtrira, pa cu da uporedjujem u reducer-u
export interface FilterObjekat
{
    gornjaCena: number,
    kategorija: string
}

export enum AkcijeNadProizvodima
{
    UCITAVANJE_PROIZVODA_IZ_BAZE = "[Akcije nad proizvodima] Ucitavanje proizvoda iz baze",
    DODAVANJE_PROIZVODA = "[Akcije nad proizvodima] Dodavanje proizvoda",
    IZMENA_PROIZVODA = "[Akcije nad proizvodima] Izmena proizvoda",
    UKLANJANJE_PROIZVODA = "[Akcije na proizvodima] Uklanjanje proizvoda",
    FILTRIRANJE_PROIZVODA = "[Akcije nad proizvodima] Filtriranje proizvoda"
}

export interface DodavanjeProizvoda
{
    type: AkcijeNadProizvodima,
    proizvod: Proizvod //akcija dodaje proizvod, A REDUCER vraca stanje zavisno od akcije
}

export interface IzmenaProizvoda
{
    type: AkcijeNadProizvodima,
    idMenjanogProizvoda: string,
    cena: number,
    opis: string
}

export interface UklanjanjeProizvoda
{
    type: AkcijeNadProizvodima,
    proizvod: Proizvod 
}
