import * as saga from 'redux-saga/effects';
import { DodavanjeProizvoda, AkcijeNadProizvodima } from './tipovi';
import { uputiZahtevKaBazi } from '../../servisi/baza.servis';
import { URLZaProizvode } from '../../servisi/servisneKonstante';
import { Proizvod } from '../../models/Proizvod';

export function* proizvodiSaga()
{
    yield saga.all([saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve()
{
    yield saga.takeEvery(AkcijeNadProizvodima.DODAVANJE_PROIZVODA, upisiProizvodUBazu);
    yield saga.takeEvery(AkcijeNadProizvodima.UKLANJANJE_PROIZVODA, ukloniProizvodIzBaze);
    yield saga.takeEvery(AkcijeNadProizvodima.IZMENA_PROIZVODA, izmeniProizvodUBazi);
}

function* upisiProizvodUBazu(akcija: DodavanjeProizvoda) //stavio sam any
{
    const { proizvod } = akcija; //mozda sam mogao direktno da dodelim, al briga me mnogo
    
    let noviProizvod: Proizvod = {
        id: proizvod.id,
        korisnickoImeVlasnika: proizvod.korisnickoImeVlasnika,
        naziv: proizvod.naziv,
        kategorija: proizvod.kategorija,
        opis: proizvod.opis,
        cena: proizvod.cena,
        putanjaDoSlike: proizvod.putanjaDoSlike
    };
    yield uputiZahtevKaBazi('POST', `${URLZaProizvode}`, noviProizvod);
}

function* ukloniProizvodIzBaze(akcija: any)
{
    const {proizvod} = akcija;
    yield uputiZahtevKaBazi('DELETE', `${URLZaProizvode}/${proizvod.id}`, null);//i ovo ce kao da radi 
}

function* izmeniProizvodUBazi(akcija: any)
{
    const {cena, opis, idMenjanogProizvoda} = akcija;
    
    const noviPodaci = {
        cena: cena,
        opis: opis
    };

    yield uputiZahtevKaBazi('PATCH', `${URLZaProizvode}/${idMenjanogProizvoda}`, noviPodaci);
}