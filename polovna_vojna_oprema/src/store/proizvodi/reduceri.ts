import { Proizvod } from "../../models/Proizvod";
import { AkcijeNadProizvodima, FilterObjekat } from "./tipovi";

export interface StanjeSvihProizvoda
{
    listaSvihProizvoda: Proizvod[],
    objekatFiltriranja: FilterObjekat
}

const pocetnoStanje: StanjeSvihProizvoda = {
    listaSvihProizvoda: [],
    objekatFiltriranja: {
        gornjaCena: 0,
        kategorija: ''
    }
}

export function reducerProizvoda(stanje = pocetnoStanje, akcija: any): StanjeSvihProizvoda
{
    switch(akcija.type)
    {
        case AkcijeNadProizvodima.DODAVANJE_PROIZVODA:
        {
            return {
                ...stanje,
                listaSvihProizvoda: [...stanje.listaSvihProizvoda, akcija.proizvod]
            }
        }

        case AkcijeNadProizvodima.UCITAVANJE_PROIZVODA_IZ_BAZE:
        {
          
            return {
                ...stanje,
                listaSvihProizvoda: [...akcija.proizvodi]
            }
        }

        case AkcijeNadProizvodima.IZMENA_PROIZVODA:
        { 
            for(let i=0; i < stanje.listaSvihProizvoda.length; i++)
            {
                if(stanje.listaSvihProizvoda[i].id === akcija.idMenjanogProizvoda)
                {
                    stanje.listaSvihProizvoda[i].cena = akcija.cena;
                    stanje.listaSvihProizvoda[i].opis = akcija.opis;
                }
            }
          
            return {
                ...stanje,
                listaSvihProizvoda: [...stanje.listaSvihProizvoda]
            }
        }

        case AkcijeNadProizvodima.FILTRIRANJE_PROIZVODA:
        {
            console.log(akcija);
            return {
                ...stanje,
                listaSvihProizvoda: [...stanje.listaSvihProizvoda],
                objekatFiltriranja: {
                    gornjaCena: akcija.filterObjekat.gornjaCena,
                    kategorija: akcija.filterObjekat.kategorija
                }
            }
        }    

        case AkcijeNadProizvodima.UKLANJANJE_PROIZVODA:
        {
            return {
                ...stanje,
                listaSvihProizvoda: stanje.listaSvihProizvoda.filter(proizvod=> proizvod.id !== akcija.proizvod.id)
            }
        }

        default:
            return stanje;
    }
}