import { combineReducers, createStore, applyMiddleware } from "redux";
import { reducerKorisnika, StanjeSvihKorisnika } from "./korisnici/reduceri";
import { reducerProizvoda, StanjeSvihProizvoda } from "./proizvodi/reduceri";
import { reducerKorisnickogInterfejsa, StanjeKorisnickogInterfejsa } from "./korisnickiInterfejs/reduceri";

import { korisnickaSaga } from "./korisnici/saga";
import { sagaAplikacije } from "./aplikacija/saga";
import { proizvodiSaga } from "./proizvodi/saga";
import createSagaMiddleware from "@redux-saga/core";

import { all } from "@redux-saga/core/effects";
import { composeWithDevTools } from 'redux-devtools-extension';

//console.log u formi za registraciju izbacuje rootReducer, zato moram da "specijalno ulazim"
//(aivtpn qwsivt)
const rootReducer = combineReducers({
    korisnici: reducerKorisnika,
    proizvodi: reducerProizvoda,
    korisnickiInterfejs: reducerKorisnickogInterfejsa
});

export interface RootAppState
{
    korisnici: StanjeSvihKorisnika,
    proizvodi: StanjeSvihProizvoda,
    korisnickiInterfejs: StanjeKorisnickogInterfejsa
};


export default function konfigurisiStore()
{
    const sagaMiddleware = createSagaMiddleware();

    const store = createStore(
        rootReducer,
        composeWithDevTools(applyMiddleware(sagaMiddleware))
    );

    sagaMiddleware.run(rootSaga);//sad je valjda povezan store sa sagom
    return store;
}

export function* rootSaga()
{
    yield all([
        korisnickaSaga(),
        proizvodiSaga(),
        sagaAplikacije()
    ]);
}