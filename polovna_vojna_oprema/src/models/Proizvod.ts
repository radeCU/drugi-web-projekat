export interface Proizvod
{
    id: string;
    korisnickoImeVlasnika: string;//odavde ide filtriranje, po ovome
    naziv: string;
    kategorija: string;//ovaj atribut moze i da preraste u enumeraciju, sutra cu da istrazim tu mogucnost
    opis?: string;
    cena: number;
    putanjaDoSlike?: string;//potencijalno zajebava
}