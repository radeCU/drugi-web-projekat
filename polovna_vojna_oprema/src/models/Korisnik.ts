export interface Korisnik
{
    id: number,
    ime: string,
    prezime: string,
    korisnickoIme: string,
    lozinka: string
}